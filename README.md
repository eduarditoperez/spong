# Spong

Spong is a game similar to Pong but with a Soccer theme.

## Installation

Download the [apk file](https://gitlab.com/eduarditoperez/spong/blob/master/Build/Android/spong-v1.0.0.apk) and install it on any android device.

## Requeriments

This game requires Android ```Lolipop``` or above.

## Game rules

1. You have 60 seconds to score as many goals as you can
2. Your paddle will shrunk during the match, so you better hurry!
3. The ball will increase his speed every time it hits a paddle (player or opponent).
4. After you or your opponent scores a goal, the ball will be place in the center of the field.
5. There's no goal limit.
6. The match will end after 60 seconds.

## How to play

1. Use your finger to control the ```Red``` paddle.


## Issues

There's still some bugs when your scrore a goal and the time runs out. 

## Contributing

This game was created with Unity 2018.4.2f1 and the source code is available for extensions.

The main scene of the game can be found at: **Assets/Scenes/Game.unity**

## Disclaimer

I don't own the assets or the music used for this game. This game is not intended for commercial purposes.

## Game modules and time effort table

This section has contains a table who hast the main modules of the game and how much time we spent in programming them,

| Module | Hours  |
| ------ | ------ |
| AI | 0.5 |
| Ball | 0.2 | 
| Field | 0.2 |
| Input | 1 |
| Managers | 2 |
| Movement | 1 |
| Player | 1 |
| Presenters | 1 |
| Timer | 0.5 |
| UI | 2 |

Also we have spend an hour or two building the ```Game``` scene, this includes:

1. Importing all the game assets (3d assets, materias, music, sprites and textures).
2. Creating the players, the field and all the prefabs.
3. Trying differents camera angles.
4. Creating tags to be used in the collision events.

We've also invest another hour testing and tunning the gameplay, bugfixing and testing the build on Android devices.

The total time invested in this project was about ```~14 hours``` splitted in 3 sessions of 4 or more hours during three days, but the fun was infinite :D

