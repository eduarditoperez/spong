﻿using com.soccerpong.field.area;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.field
{
    public class SoccerField : MonoBehaviour
    {
        [SerializeField] GoalArea blueGoalArea;
        [SerializeField] GoalArea redGoalArea;

        public string fieldName;

        #region Events
        public delegate void OnGoal(string areaColor);
        public OnGoal onGoalEvent;
        #endregion

        public void Init()
        {
            blueGoalArea.onBallEnteredListener = OnBallEnteredGoal;
            redGoalArea.onBallEnteredListener = OnBallEnteredGoal;
        }

        private void OnBallEnteredGoal(string areaColor)
        {
            //Debug.Log(string.Format("SoccerField::OnBallEnteredGoal({0})", areaColor));
            if (onGoalEvent != null)
            {
                onGoalEvent(areaColor);
            }
        }

    }
}
