﻿using com.soccerpong.ball;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.field.area
{
    public class GoalArea : MonoBehaviour
    {
        [SerializeField] string areaColor;

        #region Events
        public delegate void OnBallEntered(string areaColor);
        public OnBallEntered onBallEnteredListener;
        #endregion

        private void OnTriggerEnter(Collider other)
        {
            if (IsTheBall(other))
            {
                Debug.Log("*** The ball has entered in the goal area");
                if (onBallEnteredListener != null)
                {
                    onBallEnteredListener(areaColor);
                }
            }
        }

        private bool IsTheBall(Collider other)
        {
            return other.CompareTag("Ball");
        }
    }
}
