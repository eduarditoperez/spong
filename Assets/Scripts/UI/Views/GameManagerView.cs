﻿using com.soccerpong.presenter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.soccerpong.ui
{
    public class GameManagerView : MonoBehaviour
    {
        public Text gameTitle;
        public Text howToPlay;

        CanvasGroup viewCanvasGroup;

        #region Events
        public delegate void OnPlayButtonClicked();
        public OnPlayButtonClicked onPlayButtonClicked;
        #endregion

        private void Awake()
        {
            viewCanvasGroup = GetComponent<CanvasGroup>();
        }

        public void Play()
        {
            if (onPlayButtonClicked != null)
            {
                onPlayButtonClicked();
            }
        }

        public void ShowView()
        {
            gameObject.SetActive(true);
        }

        public void HideView()
        {
            gameObject.SetActive(false);
        }
    }
}
