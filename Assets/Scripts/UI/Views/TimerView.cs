﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour
{
    public Text timer;

    public void UpdateTimer (string timeLeft)
    {
        timer.text = string.Format("{0}", timeLeft);
    }
}
