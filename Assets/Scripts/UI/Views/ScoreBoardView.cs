﻿using com.soccerpong.ui.score.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.soccerpong.ui.score
{
    public class ScoreBoardView : MonoBehaviour
    {
        [SerializeField] private PlayerScoreView redPlayerScoreView;
        [SerializeField] private PlayerScoreView bluePlayerScoreView;

        public void UpdateScoreBoard (string redPlayerScore,
            string bluePlayerScore)
        {
            redPlayerScoreView.UpdateScore (redPlayerScore);
            bluePlayerScoreView.UpdateScore (bluePlayerScore);
        }
    }
}
