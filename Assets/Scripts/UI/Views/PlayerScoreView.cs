﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.soccerpong.ui.score.player
{

    public class PlayerScoreView : MonoBehaviour
    {
        [SerializeField] private Text playerScore;

        public void UpdateScore(string newPlayerScore)
        {
            if (playerScore != null)
            {
                playerScore.text = newPlayerScore;
            }
        }
    }
}
