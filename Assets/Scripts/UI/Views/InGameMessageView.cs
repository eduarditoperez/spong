﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.soccerpong.ui
{
    /// <summary>
    /// This class will be used to show messages during
    /// the game.
    /// </summary>
    public class InGameMessageView : MonoBehaviour
    {
        public Text message;

        CanvasGroup viewCanvas;

        private void Awake()
        {
            viewCanvas = GetComponent<CanvasGroup>();
            message.text = string.Empty;
        }

        public void ShowMessage (string messageToShow)
        {
            viewCanvas.alpha = 1;
            message.text = messageToShow;
            StartCoroutine(WaitAndHideMessage(2f));
        }

        IEnumerator WaitAndHideMessage(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            viewCanvas.alpha = 0;
        }
    }
}
