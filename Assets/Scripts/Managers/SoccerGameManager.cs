﻿using com.soccerpong.ball;
using com.soccerpong.field;
using com.soccerpong.input;
using com.soccerpong.npc;
using com.soccerpong.player;
using com.soccerpong.presenter;
using com.soccerpong.timer;
using com.soccerpong.ui;
using com.soccerpong.ui.score;
using System;
using System.Collections;
using UnityEngine;

namespace com.soccerpong.manager
{
    /// <summary>
    /// This class contains all the logic
    /// for a soccer match.
    /// </summary>
    public class SoccerGameManager : MonoBehaviour
    {
        [Header("Game Players")]
        [SerializeField] private SoccerNPC bluePlayer;
        [SerializeField] private SoccerPlayer redPlayer;

        [Header("Game ball")]
        [SerializeField] private SoccerBall ball;

        [Header("Game Field")]
        [SerializeField] private SoccerField soccerField;

        [Header("Score Keepper")]
        [SerializeField] private ScoreManager scoreManager;

        [Header("Game Timer")]
        [SerializeField] private SoccerTimer timer;

        [Header("Score Board")]
        [SerializeField] private ScoreBoardView scoreBoardView;

        [Header("GameManager View")]
        [SerializeField] private GameManagerView gameManagerView;

        [Header("Timer View")]
        [SerializeField] private TimerView timerView;

        [Header("In Game Message View")]
        [SerializeField] private InGameMessageView inGameMessageView;

        // Input handler
        private InputHandler redPlayerInputHandler;

        // Presenters
        private ScorePresenter scorePresenter;
        private GameManagerPresenter gameManagerPresenter;
        private TimerPresenter timerPresenter;
        private InGameMessagePresenter inGameMessagePresenter;

        public bool gameStarted;

        #region Monobehaviour
        private void Start()
        {
            CreateNewGame();
        }

        private void Update()
        {
            if (gameStarted)
            {
                timerPresenter.UpdateTimer();
            }
        }
        #endregion

        #region Init
        public void CreateNewGame ()
        {
            Debug.Log("SoccerGameManager::CreateNewGame()");
            InitGameManagerPresenter();
            InitScoreBoard();
            InitSoccerField();
            InitPlayers();
            InitTimer();
            InitMessagePresenter();
        }

        private void InitGameManagerPresenter()
        {
            gameManagerPresenter = new GameManagerPresenter(gameManagerView, this);
        }

        private void InitScoreBoard()
        {
            scorePresenter = new ScorePresenter(scoreBoardView, scoreManager);
            ResetScoreBoardView();
        }

        private void ResetScoreBoardView()
        {
            scorePresenter.ResetScoreBoard();
        }

        private void InitTimer()
        {
            timer.Init(60);
            timer.onTimeIsUp = OnGameTimeIsUp;
            timerPresenter = new TimerPresenter(timer, timerView);
        }

        private void InitSoccerField()
        {
            soccerField.Init();
            soccerField.onGoalEvent = WhenBallHasEnteredAGoalZone;
        }

        private void InitPlayers()
        {
            bluePlayer.Init("bluePlayer");
            redPlayer.Init("redPlayer", ProvideInputHandler());
        }

        private InputHandler ProvideInputHandler()
        {
            MobileInputHandler mobileInputHandler = FindObjectOfType<MobileInputHandler>();
            return mobileInputHandler;
//#if UNITY_EDITOR
//            EditorInputHandler editorInputHandler = FindObjectOfType<EditorInputHandler>();
//            return editorInputHandler;
//#else
//            MobileInputHandler mobileInputHandler = FindObjectOfType<MobileInputHandler>();
//            return mobileInputHandler;
//#endif
        }

        private void InitMessagePresenter()
        {
            inGameMessagePresenter = new InGameMessagePresenter(inGameMessageView);
        }
#endregion

#region Game events
        public void StartGame()
        {
            StartCoroutine(WaitAndStartTheGame());
        }

        private void KickOff()
        {
            redPlayer.UnlockPlayer();
            ball.Move();
            timer.StartTimer();
            gameStarted = true;
        }

        private void WhenBallHasEnteredAGoalZone (string goalColor)
        {
            ShowIngameMessage("Goal!!!");
            UpdatePlayerScore (goalColor);
            ResetGameAfteAGoal();
        }

        private void ShowIngameMessage(string gameMessage)
        {
            inGameMessagePresenter.ShowMessage(gameMessage);
        }

        private void UpdatePlayerScore (string goalColor)
        {
            if (goalColor.Equals("red"))
            {
                scoreManager.UpdatePlayerScore("blue");
            }

            if (goalColor.Equals("blue"))
            {
                scoreManager.UpdatePlayerScore("red");
            }

            scorePresenter.UpdateScoreBoard();
        }

        private void ResetGameAfteAGoal()
        {
            StartCoroutine(WaitAndResetTheBall(3f));
        }

        public void ResetGame()
        {
            scoreManager.ResetScoreBoard();
            ResetScoreBoardView();
        }

        void OnGameTimeIsUp ()
        {
            gameStarted = false;
            ball.Stop();
            bluePlayer.Stop();
            redPlayer.ResetPlayer();
            timer.StopTimer();
            timer.ResetTimer();
            scoreManager.ResetScoreBoard();
            ShowIngameMessage("Game Over");
            StartCoroutine(WaitAndReStartTheGame());
        }
#endregion

#region Coroutines
        IEnumerator WaitAndStartTheGame()
        {
            ShowIngameMessage("3");
            yield return new WaitForSeconds(1f);
            ShowIngameMessage("2");
            yield return new WaitForSeconds(1f);
            ShowIngameMessage("1");
            yield return new WaitForSeconds(1f);
            KickOff();
        }

        IEnumerator WaitAndResetTheBall(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            ball.ResetPosition();
            yield return new WaitForSeconds(1f);
            KickOff();
        }

        IEnumerator WaitAndReStartTheGame()
        {
            yield return new WaitForSeconds(3f);
            gameManagerPresenter.ShowMainMenu();
        }
        #endregion
    }
}
