﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] int redScore;
        [SerializeField] int blueScore;

        public int BlueScore { get => blueScore; set => blueScore = value; }
        public int RedScore { get => redScore; set => redScore = value; }

        public void ResetScoreBoard()
        {
            BlueScore = 0;
            RedScore = 0;
        }

        public void UpdatePlayerScore(string playerColor)
        {
            if (playerColor.Equals("red"))
            {
                RedScore++;
            }

            if (playerColor.Equals("blue"))
            {
                BlueScore++;
            }
        }
    }
}
