﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.timer
{
    /// <summary>
    /// A simple class which is use to represent the time left
    /// inside a match.
    /// </summary>
    public class SoccerTimer : MonoBehaviour
    {
        private float timer;
        private float currentTimer;
        private bool timerStarted;

        // Timer Events
        public delegate void OnTimeIsUp();
        public OnTimeIsUp onTimeIsUp;

        // Start is called before the first frame update
        public void Init(float timeToCountDown)
        {
            this.timer = timeToCountDown;
            ResetTimer();
            StopTimer();
        }

        public void StartTimer()
        {
            timerStarted = true;
        }

        public void StopTimer()
        {
            timerStarted = false;
        }

        public void ResetTimer()
        {
            currentTimer = timer;
        }

        public float GetTimeLeft() {
            if (currentTimer > 0) {
                return currentTimer;
            }
            return 0;
        }

        void Update()
        {
            if (timerStarted)
            {
                UpdateTimer();
            }
        }

        private void UpdateTimer()
        {
            if (currentTimer > 0)
            {
                currentTimer -= Time.deltaTime;
                return;
            }

            StopTimer();
            ResetTimer();
            TimeIsUp();
        }

        private void TimeIsUp()
        {
            if (onTimeIsUp != null)
            {
                onTimeIsUp();
            }
        }

    }
}
