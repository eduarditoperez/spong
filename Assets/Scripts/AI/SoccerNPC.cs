﻿using com.soccerpong.movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.npc
{
    /// <summary>
    /// SoccerNPC represent the opponents.
    /// </summary>
    public class SoccerNPC : MonoBehaviour, Movable
    {
        [SerializeField] private string npcName;
        [SerializeField] private float npcSpeed;

        private bool canMove;

        public void Init(string npcName)
        {
            this.npcName = npcName;
            RandomizeSpeed();
            Stop();
        }

        public void Move()
        {
            canMove = true;
        }

        public void Stop()
        {
            canMove = false;
        }

        public void UpdatePosition(Vector3 newPosition)
        {
            transform.position = newPosition;
        }

        public void Bounce(Vector3 direction)
        {
            //nothing
        }

        public void RandomizeSpeed()
        {
            this.npcSpeed = Random.RandomRange(0.5f, 1.5f);
        }

        void Update()
        {
            UpdatePosition(
                new Vector3(Mathf.Lerp(-4, 4, Mathf.PingPong(Time.time * npcSpeed, 1)),
                transform.position.y,
                transform.position.z));
        }

        public void IncreaseSpeed()
        {
            
        }

        public void DecreaseSpeed()
        {
            
        }

        public void ResetSpeed()
        {
            
        }
    }
}
