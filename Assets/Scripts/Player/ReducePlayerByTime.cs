﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.player
{
    /// <summary>
    /// This class reduce the player over the time
    /// </summary>
    public class ReducePlayerByTime : MonoBehaviour
    {
        public bool startReduce;

        private Vector3 originalScale;

        public void StartReduce()
        {
            startReduce = true;
        }

        public void ResetSize()
        {
            transform.localScale = originalScale;
        }

        public void StopReduce()
        {
            startReduce = false;
        }

        #region Monobehaviour
        // Start is called before the first frame update
        void Start()
        {
            startReduce = false;
            originalScale = transform.localScale;
        }

        // Update is called once per frame
        void Update()
        {
            if (startReduce)
            {
                Vector3 newScale = transform.localScale - (Vector3.right * Time.deltaTime * 0.05f);
                transform.localScale = newScale;
                if (HasReachedMinSize())
                {
                    StopReduce();
                }
            }
        }

        private bool HasReachedMinSize()
        {
            return transform.localScale.x <= 2f;
        }


        #endregion
    }
}
