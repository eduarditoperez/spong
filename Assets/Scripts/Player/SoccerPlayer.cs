﻿using com.soccerpong.input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.player
{
    public class SoccerPlayer : MonoBehaviour
    {
        [SerializeField] string playerName;
        [Range(0, 0.9f)] public float speed;

        InputHandler playerInputHandler;
        bool inited;
        ReducePlayerByTime playerReducer;

        public void Init(string playerName, InputHandler playerInputHandler)
        {
            this.playerInputHandler = playerInputHandler;
            this.playerReducer = GetComponent<ReducePlayerByTime>();
        }

        public void UnlockPlayer()
        {
            inited = true;
            playerReducer.StartReduce();
        }

        public void ResetPlayer()
        {
            playerReducer.ResetSize();
        }

        private void Awake()
        {
            inited = false;
        }

        private void Update()
        {
            if (inited)
            {
                var direction = playerInputHandler.GetDirection();
                //Debug.Log(direction);
                UpdatePosition(direction);
            }
        }

        private void UpdatePosition (Vector2 movementDirection)
        {
            // Note: this may be confusing, we use the movement
            // of the finger from up/down to move the player
            // along the X-axis, and here we do the conversion
            // from an inputdirection to a ingame movement.
            float moveHorizontal = -movementDirection.y;

            Vector3 currentPosition = transform.position;
            Vector3 position = new Vector3
            (
                // TODO: still we need to pass the boundaries of
                // the field as parameters.
                Mathf.Clamp((currentPosition.x + moveHorizontal) * speed, -5.5f, 5.5f),
                currentPosition.y,
                currentPosition.z
            );

            this.transform.position = position;
        }
    }
}
