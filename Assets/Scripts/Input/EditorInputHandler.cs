﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.input
{
    /// <summary>
    /// This class is a helper to test the player movement
    /// in the editor.
    /// </summary>
    public class EditorInputHandler : MonoBehaviour, InputHandler
    {
        private Vector2 direction;

        private void Awake()
        {
            direction = Vector2.zero;
        }

        private void Update()
        {
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
        }

        public Vector2 GetDirection()
        {
            return direction;
        }
    }
}
