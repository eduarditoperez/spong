﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace com.soccerpong.input
{
    public interface InputHandler 
    {
        Vector2 GetDirection();
    }
}
