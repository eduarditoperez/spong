﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace com.soccerpong.input
{
    /// <summary>
    /// This class convert raw input into Vector2 normalized
    /// directions be used by the player.
    /// </summary>
    public class MobileInputHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, InputHandler
    {
        public float smoothing;

        private Vector2 origin;
        private Vector2 direction;
        private Vector2 smoothDirection;
        private bool touched;
        private int pointerID;

        void Awake()
        {
            direction = Vector2.zero;
            touched = false;
        }

        public void OnPointerDown (PointerEventData data)
        {
            if (!touched)
            {
                touched = true;
                pointerID = data.pointerId;
                origin = data.position;
            }
        }

        public void OnDrag (PointerEventData data)
        {
            if (data.pointerId == pointerID)
            {
                Vector2 currentPosition = data.position;
                Vector2 directionRaw = currentPosition - origin;
                direction = directionRaw.normalized;
            }
        }

        public void OnPointerUp (PointerEventData data)
        {
            if (data.pointerId == pointerID)
            {
                direction = Vector3.zero;
                touched = false;
            }
        }

        public Vector2 GetDirection ()
        {
            return direction;
        }

    }
}
