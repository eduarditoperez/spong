﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.movement
{
    public interface Movable
    {
        void Move();
        void Stop();
        void Bounce(Vector3 direction);
        void UpdatePosition(Vector3 position);
        void RandomizeSpeed();
        void IncreaseSpeed();
        void DecreaseSpeed();
        void ResetSpeed();
    }
}
