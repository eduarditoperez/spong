﻿using UnityEngine;

namespace com.soccerpong.movement
{
    public class BallMovement : MonoBehaviour, Movable
    {
        private static readonly float speedIncrement = 1f;

        [SerializeField] private float minSpeed = 10f;
        [SerializeField] private float maxSpeed = 25f;
        [SerializeField] private bool move;

        //private Vector3 initialVelocityVector;
        private Vector3 lastFrameVelocity;
        private Rigidbody ballRigidBody;

        private float speedFactor = 0;

        private void Awake()
        {
            ballRigidBody = GetComponent<Rigidbody>();
            move = false;
        }

        private void Update()
        {
            if (move)
            {
                lastFrameVelocity = ballRigidBody.velocity;
            }
        }

        public void Move()
        {
            move = true;
            UpdateVelocity (1, CreateRandomVelocityVector());
        }

        private Vector3 CreateRandomVelocityVector()
        {
            int randomX = Random.Range(-1, 2);
            return new Vector3 (randomX == 0 ? 1 : randomX, 0f, -1f);
        }

        public void Stop()
        {
            move = false;
            ballRigidBody.velocity = Vector3.zero;
        }

        public void Bounce(Vector3 collisionNormal)
        {
            var speed = lastFrameVelocity.magnitude;
            var direction = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);
            UpdateVelocity (speed, direction);
        }

        private void UpdateVelocity (float speed, Vector3 direction)
        {
            ballRigidBody.velocity = direction * 
                Mathf.Min (maxSpeed, Mathf.Max(speed + speedFactor, minSpeed));
        }

        public void UpdatePosition(Vector3 newPosition)
        {
            transform.position = newPosition;
        }

        public void RandomizeSpeed()
        {
            // nothing to be done here
        }

        public void IncreaseSpeed()
        {
            speedFactor += speedIncrement;
            if (speedFactor > maxSpeed)
            {
                speedFactor = maxSpeed;
            }
        }

        public void DecreaseSpeed()
        {
            speedFactor -= speedIncrement;
            if (speedFactor < 0) {
                speedFactor = 0;
            }
        }

        public void ResetSpeed()
        {
            speedFactor = 0;
        }

        #region Collision Handling
        private void OnCollisionEnter(Collision collision)
        {
            if (HasCollidedWithAPlayer(collision))
            {
                IncreaseSpeed();
            }
            Bounce(collision.GetContact(0).normal);
        }

        private bool HasCollidedWithAPlayer(Collision collision)
        {
            return collision.gameObject.CompareTag("Player");
        }

        private void OnTriggerEnter(Collider other)
        {
            if (IsGoalTrigger(other))
            {
                Stop();
            }
        }

        private bool IsGoalTrigger(Collider other) {
            return other.CompareTag("BlueGoal") ||
                other.CompareTag("RedGoal");
        }
        #endregion


    }
}