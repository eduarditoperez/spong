﻿using com.soccerpong.movement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.ball
{
    public class SoccerBall : MonoBehaviour
    {
        public string soccerBallName;

        BallMovement ballMovement;

        private Vector3 initialPosition;

        private void Awake()
        {
            ballMovement = GetComponent<BallMovement>();
            initialPosition = transform.position;
        }

        internal void ResetPosition()
        {
            Debug.Log("SoccerBall::ResetPosition()");
            ballMovement.UpdatePosition(initialPosition);
        }

        internal void Move()
        {
            ballMovement.Move();
        }

        internal void Stop()
        {
            ballMovement.Stop();
            ballMovement.ResetSpeed();
        }
    }
}
