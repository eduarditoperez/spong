﻿using com.soccerpong.ui;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.presenter
{
    public class InGameMessagePresenter
    {
        InGameMessageView messageView;

        public InGameMessagePresenter (InGameMessageView messageView)
        {
            this.messageView = messageView;
        }

        public void ShowMessage(string message)
        {
            messageView.ShowMessage(message);
        }
    }
}
