﻿using com.soccerpong.manager;
using com.soccerpong.ui;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.presenter
{
    public class GameManagerPresenter
    {
        GameManagerView gameManagerView;
        SoccerGameManager soccerGameManager;

        public GameManagerPresenter(GameManagerView gameManagerView,
            SoccerGameManager soccerGameManager)
        {
            this.gameManagerView = gameManagerView;
            this.gameManagerView.onPlayButtonClicked = OnPlayButtonClicked;
            this.soccerGameManager = soccerGameManager;
        }

        public void OnPlayButtonClicked()
        {
            gameManagerView.HideView();
            soccerGameManager.StartGame();
        }

        internal void ShowMainMenu()
        {
            gameManagerView.ShowView();
        }
    }
}
