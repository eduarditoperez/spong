﻿using com.soccerpong.manager;
using com.soccerpong.ui.score;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.soccerpong.presenter
{
    /// <summary>
    /// ScorePresenter is responsible to update the score
    /// in the ui. This follows the Model View Presenter 
    /// pattern.
    /// </summary>
    public class ScorePresenter
    {
        private ScoreBoardView scoreBoardView;
        private ScoreManager scoreManager;

        public ScorePresenter(ScoreBoardView scoreBoardView,
            ScoreManager scoreManager)
        {
            this.scoreBoardView = scoreBoardView;
            this.scoreManager = scoreManager;
        }

        public void UpdateScoreBoard()
        {
            scoreBoardView.UpdateScoreBoard(
                scoreManager.RedScore.ToString(),
                scoreManager.BlueScore.ToString());
        }

        public void ResetScoreBoard()
        {
            scoreBoardView.UpdateScoreBoard("0", "0");
        }
    }
}
