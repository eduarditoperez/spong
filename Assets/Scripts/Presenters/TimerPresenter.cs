﻿using com.soccerpong.timer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.soccerpong.presenter
{
    public class TimerPresenter
    {
        TimerView timerView;
        SoccerTimer soccerTimer;

        public TimerPresenter(SoccerTimer timer, TimerView view)
        {
            this.soccerTimer = timer;
            this.timerView = view;
        }

        public void UpdateTimer()
        {
            timerView.UpdateTimer(soccerTimer.GetTimeLeft().ToString("00"));
        }
    }
}
